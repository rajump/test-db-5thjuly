﻿-- <Migration ID="9a0803ff-687f-4091-b940-22cd638feb56" />
GO

PRINT N'Creating [dbo].[EMP]'
GO
CREATE TABLE [dbo].[EMP]
(
[empno] [int] NOT NULL,
[ename] [varchar] (10) NULL,
[job] [varchar] (9) NULL,
[mgr] [int] NULL,
[hiredate] [datetime] NULL,
[sal] [numeric] (7, 2) NULL,
[comm] [numeric] (7, 2) NULL,
[dept] [int] NULL,
[address] [nchar] (10) NULL
)
GO
PRINT N'Creating primary key [PK__EMP__AF4C318A2BD601AD] on [dbo].[EMP]'
GO
ALTER TABLE [dbo].[EMP] ADD CONSTRAINT [PK__EMP__AF4C318A2BD601AD] PRIMARY KEY CLUSTERED ([empno])
GO
